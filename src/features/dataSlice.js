import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  screen: {
    stepOne: true,
    stepTwo: false,
    stepThird: false,
    stepFourth: false,
  },
  floors: null,
  appartments: '',
  material: '',
};

const screenSlice = createSlice({
  name: 'data',
  initialState,
  reducers: {
    nextScreen(state, action) {
      const screen = action.payload;
      for (let x in state.screen) {
        state.screen[x] = false;
        if (x === screen) {
          state.screen[x] = true;
        }
      }
      return state;
    },
    setAppartments(state, action) {
      const appartments = action.payload;
      state.appartments = appartments;
      return state;
    },
    setMaterial(state, action) {
      const material = action.payload;
      state.material = material;
      return state;
    },
    setFloors(state, action) {
      const floors = action.payload;
      state.floors = floors;
      return state;
    },
    cancel(state) {
      state = initialState;
      return state;
    },
  },
});

export const {
  nextScreen,
  setAppartments,
  setMaterial,
  setFloors,
  cancel
} = screenSlice.actions;

export default screenSlice.reducer;
