import { useSelector } from 'react-redux';
import StepOne from './screens/StepOne';
import StepTwo from './screens/StepTwo';
import StepThird from './screens/StepThird';
import StepFourth from './screens/StepFourth';
import './App.scss';

function App() {
  const screen = useSelector((state) => state.data.screen);

  return (
    <div className="App">
      <span className="title">Калькулятор цены конструкций</span>
      {screen.stepOne ? (
        <StepOne />
      ) : screen.stepTwo ? (
        <StepTwo />
      ) : screen.stepThird ? (
        <StepThird />
      ) : (
        <StepFourth />
      )}
    </div>
  );
}

export default App;
