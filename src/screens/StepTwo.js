import { useState } from 'react';
import { cancel, nextScreen, setFloors } from '../features/dataSlice';
import { useDispatch } from 'react-redux';

const StepTwo = () => {
  const dispatch = useDispatch();

  const [floors, setFloorsLocally] = useState('');

  const [validateStyling, setValidateStyling] = useState({});

  const validate = (matched) => {
    if (matched !== null) {
      setValidateStyling({ borderColor: 'inherit' });
    } else {
      setValidateStyling({ borderColor: 'red' });
    }
  };

  const conditionToNext =
    floors === '' || validateStyling.borderColor === 'red';

  const nextScreenHandler = () => {
    dispatch(nextScreen('stepThird'));
    dispatch(setFloors(floors));
  };

  return (
    <>
      <span className="subtitle">Шаг 2</span>
      <div className="calc__box">
        <span className="calc__header">Количество этажей число?</span>
        <div className="calc__floors">
          <input
            value={floors}
            style={validateStyling}
            onChange={(e) => {
              setFloorsLocally(e.target.value);
              validate(e.target.value.match(/^\d+$/gi));
            }}
            className="calc__input"
            type="text"
          />
        </div>
      </div>
      <div className="buttons__box">
        <div onClick={() => dispatch(cancel())} className="buttons__btn">
          Отмена
        </div>
        <div
          className={`buttons__btn ${
            conditionToNext && 'buttons__btn_disabled'
          }`}
          onClick={() => {
            if (!conditionToNext) {
              nextScreenHandler();
            }
          }}
        >
          Далее
        </div>
      </div>
    </>
  );
};

export default StepTwo;
