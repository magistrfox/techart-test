import { cancel, nextScreen, setMaterial } from '../features/dataSlice';
import { useDispatch, useSelector } from 'react-redux';

const StepThird = () => {
  const dispatch = useDispatch();

  const nextScreenHandler = (material) => {
    dispatch(nextScreen('stepFourth'));
    dispatch(setMaterial(material));
  };

  const appartments = useSelector((state) => state.data.appartments);

  const materials =
    appartments === 'Жилой дом'
      ? ['Кирпич', 'Шлакоблок', 'Деревянный брус']
      : ['Шлакоблок', 'Металл', 'Сендвич-панели'];

  return (
    <>
      <span className="subtitle">
        {appartments === 'Жилой дом' ? 'Шаг 3' : 'Шаг 2'}
      </span>
      <div className="calc__box">
        <span className="calc__header">Материал стен</span>
        <ul className="calc__list">
          {materials.map((material, i) => (
            <li key={i} onClick={() => nextScreenHandler(material)}>
              {material}
            </li>
          ))}
        </ul>
      </div>
      <div className="buttons__box">
        <div onClick={() => dispatch(cancel())} className="buttons__btn">
          Отмена
        </div>
        <div className="buttons__btn buttons__btn_disabled">Далее</div>
      </div>
    </>
  );
};

export default StepThird;
