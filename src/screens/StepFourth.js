import { useState } from 'react';
import { cancel } from '../features/dataSlice';
import { useDispatch, useSelector } from 'react-redux';

const StepFourth = () => {
  const dispatch = useDispatch();

  const [length, setLength] = useState({
    x: '',
    y: '',
  });

  const [validateStyling, setValidateStyling] = useState({
    x: '',
    y: '',
  });

  const validate = (matched, input) => {
    if (matched !== null) {
      setValidateStyling((state) => {
        return { ...state, [input]: 'inherit' };
      });
    } else {
      setValidateStyling((state) => {
        return { ...state, [input]: 'red' };
      });
    }
  };

  const data = useSelector((state) => state.data);

  const conditionToCount =
    validateStyling.x === '' ||
    validateStyling.y === '' ||
    validateStyling.x === 'red' ||
    validateStyling.y === 'red';

  const [response, setResponse] = useState({
    title: data.appartments === 'Жилой дом' ? 'Шаг 4' : 'Шаг 3',
    body: null,
    error: null,
  });

  const countHandler = () => {
    let url = `https://data.techart.ru/lab/json/`;
    const building = data.appartments === 'Жилой дом' ? '1' : '2';
    const height = data.appartments === 'Гараж' ? '' : data.floors;
    const material =
      data.material === 'Кирпич'
        ? '1'
        : data.material === 'Шлакоблок'
        ? '2'
        : data.material === 'Деревянный брус'
        ? '3'
        : data.material === 'Металл'
        ? '4'
        : '5';
    const sizex = length.x;
    const sizey = length.y;
    url = `${url}?building=${building}&height=${height}&material=${material}&sizex=${sizex}&sizey=${sizey}`;
    fetch(url)
      .then((data) => data.json())
      .then((res) => {
        if (res.result === 'error') {
          setResponse({
            error: true,
            title: 'Результаты расчета',
            body: res.message,
          });
        } else {
          setResponse({ title: 'Результаты расчета', body: res.message });
        }
      });
  };

  return (
    <>
      <span className="subtitle">{response.title}</span>
      <div className="calc__box">
        <span className="calc__header">
          {response.error
            ? 'Ошибка'
            : response.body
            ? 'Успешно'
            : 'Длина стен (в метрах)'}
        </span>
        <div className="calc__length">
          {response.body ? (
            <span
              className={`response__body ${
                response.error && 'response__body_error'
              }`}
            >
              {response.body}
            </span>
          ) : (
            <>
              <input
                value={length.x}
                style={{ borderColor: validateStyling.x }}
                onChange={(e) => {
                  setLength((state) => {
                    return { ...state, x: e.target.value };
                  });
                  validate(e.target.value.match(/^\d+$/gi), 'x');
                }}
                className="calc__input"
                type="text"
              />
              X
              <input
                value={length.y}
                style={{ borderColor: validateStyling.y }}
                onChange={(e) => {
                  setLength((state) => {
                    return { ...state, y: e.target.value };
                  });
                  validate(e.target.value.match(/^\d+$/gi), 'y');
                }}
                className="calc__input"
                type="text"
              />
            </>
          )}
        </div>
      </div>
      <div className="buttons__box">
        {response.body ? (
          <div
            style={{ width: '200px' }}
            onClick={() => dispatch(cancel())}
            className="buttons__btn"
          >
            Новый расчет
          </div>
        ) : (
          <>
            <div onClick={() => dispatch(cancel())} className="buttons__btn">
              Отмена
            </div>
            <div
              className={`buttons__btn ${
                conditionToCount && 'buttons__btn_disabled'
              }`}
              onClick={() => {
                if (!conditionToCount) {
                  countHandler();
                }
              }}
            >
              Рассчитать
            </div>
          </>
        )}
      </div>
    </>
  );
};

export default StepFourth;
