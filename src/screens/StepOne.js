import { nextScreen, setAppartments } from '../features/dataSlice';
import { useDispatch } from 'react-redux';

const StepOne = () => {
  const dispatch = useDispatch();

  const nextScreenHandler = (building) => {
    if (building === 'house') {
      dispatch(nextScreen('stepTwo'));
      dispatch(setAppartments('Жилой дом'));
    } else {
      dispatch(nextScreen('stepThird'));
      dispatch(setAppartments('Гараж'));
    }
  };

  return (
    <>
      <span className="subtitle">Шаг 1</span>
      <div className="calc__box">
        <span className="calc__header">Что будем строить?</span>
        <ul className="calc__list">
          <li onClick={() => nextScreenHandler('house')}>Жилой дом</li>
          <li onClick={() => nextScreenHandler('garage')}>Гараж</li>
        </ul>
      </div>
      <div className="buttons__box">
        <div className="buttons__btn buttons__btn_disabled">Далее</div>
      </div>
    </>
  );
};

export default StepOne;
